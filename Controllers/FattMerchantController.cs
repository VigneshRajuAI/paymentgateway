﻿using ExamRoom.PaymentGateways.Services;
using ExamRoom.PaymentGateways.Services.FattMerchant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamRoom.PaymentGateways.Controllers
{
    [Route("api/FattMerchant")]
    [ApiController]
    public class FattMerchantController : ControllerBase
    {
        private readonly PaymentService paymentService;
        private readonly AppSettings settings;
        public FattMerchantController(PaymentService paymentService, IOptions<AppSettings> settings)
        {
            this.paymentService = paymentService;
            this.settings = settings.Value;
        }

        public IOptions<AppSettings> Settings { get; }

        [HttpPost]
        [Route("CreatePaymentmethod")]
        public async Task<ActionResult> CreatePaymentmethod([FromBody] CreatePaymentMethodRequest request)
        {
            var result = new CreatePaymentMethodResponse();
            try
            {
                result = await paymentService.Createpaymentmethod(request);
                return Ok(result);
            }
            catch (Exception ex)
            {

            }
            return Ok(result);
        }
    }
}
