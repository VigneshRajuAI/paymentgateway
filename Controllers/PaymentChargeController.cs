﻿using ExamRoom.PaymentGateways.Services;
using ExamRoom.PaymentGateways.Services.FattMerchant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamRoom.PaymentGateways.Controllers
{
    [Route("api/FattMerchant")]
    [ApiController]
    public class PaymentChargeController : Controller
    {
        private readonly PaymentService paymentService;
        private readonly AppSettings settings;
        public PaymentChargeController(PaymentService paymentService, IOptions<AppSettings> settings)
        {
            this.paymentService = paymentService;
            this.settings = settings.Value;
        }

        [HttpPost]
        [Route("Paymentcharge")]
        public async Task<ActionResult> CreatePaymentPaymentcharge([FromBody] PaymentChargeRequest request)
        {
            var result = new PaymentChargeResponse();
            try
            {
                result = await paymentService.Createpaymentcharge(request);
            }
            catch(Exception ex)
            {

            }
            return Ok(result);
            
        }
    }
}
