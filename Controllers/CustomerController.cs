﻿using ExamRoom.PaymentGateways.Services;
using ExamRoom.PaymentGateways.Services.FattMerchant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamRoom.PaymentGateways.Controllers
{
    [Route("api/FattMerchant")]
    [ApiController]
    public class CustomerController : Controller
    {

        private readonly PaymentService paymentService;
        private readonly AppSettings settings;
        public CustomerController(PaymentService paymentService, IOptions<AppSettings> settings)
        {
            this.paymentService = paymentService;
            this.settings = settings.Value;
        }

        [HttpPost]
        [Route("Createcustomer")]
        public async Task<ActionResult> CreatePaymentmethod([FromBody] CreateCustomerRequest request)
        {
            var result = new CreateCustomerResponse();
            try
            {
                result = await paymentService.CreateCustomermethod(request);
            }
            catch(Exception ex)
            {

            }
            return Ok(result);
        }
    }
}
